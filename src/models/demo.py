import os
from datetime import datetime
from pymodm import MongoModel, fields

from bson.objectid import ObjectId


class Model(MongoModel):
    class Meta:
        collection_name = 'demo'
        final = True

    _id = fields.ObjectIdField(primary_key=True)
    name = fields.CharField(blank=True)
    note = fields.CharField(blank=True)
    # value = fields.OrderedDictField(required=True)


class ModelManager(object):
    @staticmethod
    def add(obj):
        return Model(
            _id=obj['_id'],
            name=obj['name'],
            # value=obj['value'],
            note=obj['note'],
        ).save()

    @staticmethod
    def update(obj_new):
        old_id = obj_new['_id']
        del obj_new['_id']
        return Model.objects.raw({'_id':  ObjectId(old_id)}).update({'$set': obj_new})

    @staticmethod
    def delete(_id):
        return Model.objects.get({'_id': ObjectId(_id)}).delete()

    @staticmethod
    def get_by_id(_id):
        return Model.objects.get({'_id': ObjectId(_id)})

    @staticmethod
    def get_list(_filter, _sort, page, page_size):
        _skip = (page - 1) * page_size
        total = Model.objects.raw(_filter)
        datas = Model.objects.raw(_filter).order_by(_sort) \
            .skip(_skip).limit(page_size)
        return datas, total

    @staticmethod
    def get_all():
        return Model.objects.raw({})
