import os
import urllib3
from flask import Flask
from flask_cors import CORS
from pymodm import connect

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from .api import *
from .configs import AppConfig
from .extensions import redis

# for import *
__all__ = ['create_app']

DEFAULT_BLUEPRINT = [
    api_v1,
    api_v2
]


def create_app(config=None, app_name=None):
    if app_name is None:
        app_name = AppConfig.PROJECT

    app = Flask(app_name)

    configure_app(app, config)
    configure_extensions(app)
    configure_blueprints(app)

    return app


def configure_app(app, config):
    app.config.from_object(AppConfig)
    if config:
        app.config.from_object(config)


def configure_blueprints(app, blueprints=None):
    if not blueprints:
        blueprints = DEFAULT_BLUEPRINT

    for bp in blueprints:
        app.register_blueprint(bp)


def configure_extensions(app):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    cors = CORS(app, resources={r"*": {"origins": "*"}})

    # init redis
    redis.init_app(app, config_prefix='APP_REDIS')
    # init mongodb
    connect(AppConfig.APP_MONGODB, connect=False)
    # init sentry
    sentry_sdk.init(
        dsn=AppConfig.SENTRY_DSN,
        integrations=[FlaskIntegration()]
    )
