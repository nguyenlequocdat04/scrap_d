from flask_restplus import Namespace, fields

from .api import BaseModule


class Module(BaseModule):

    api = Namespace('Demo resource', description='Demo api')

    model_post = api.model('model post', {
        'name': fields.String(description='foo'),
        'note': fields.String(description='bar'),
        'value': fields.Raw(),
    })

    model = api.clone('Demo', model_post, {
        'id': fields.String(description='The identifier', attribute='_id'),
        'avt': fields.String(description='foo', attribute='name'),
    })

    model_list = api.model('List Return', {
        'data': fields.List(fields.Nested(model)),
        'total': fields.Integer()
    })

    SORT_TYPE_VALUES = ('lastest', 'oldest')

    api_parser = api.parser()
    api_parser.add_argument('page', type=int, help='default: 1')
    api_parser.add_argument('page_size', type=int, help='default: 20')
    api_parser.add_argument(
        'sort_type', choices=SORT_TYPE_VALUES, help='default is lastest'
    )
