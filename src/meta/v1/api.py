from flask import Blueprint
from flask_restplus import Api, Namespace, fields


class BaseModule:
    AUTHEN_BY_TOKEN = 'authorization'
    AUTHEN_BY_KEY = 'apikey'

    AUTHORIZATIONS = {
        AUTHEN_BY_TOKEN: {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        },
        AUTHEN_BY_KEY: {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-API-KEY'
        }
    }

    RESPONSE_CODE = {
        200: 'Success',
        202: 'Accepted - Worker will do it',
        400: 'Validation Error',
        401: 'Token is required',
        403: 'This user is blocked | permision denied',
        404: 'Data not found',
        423: 'This resource is locked by admin',
        429: 'Too Many Requests',
    }


class ApiModule(BaseModule):
    blueprint = Blueprint('api v1', __name__, url_prefix='/api/v1')
    api = Api(
        blueprint,
        title='API SERVICES SAMPLE 1',
        version='3.0',
        description='The sample api docs!',
        security=[BaseModule.AUTHEN_BY_KEY, BaseModule.AUTHEN_BY_TOKEN],
        authorizations=BaseModule.AUTHORIZATIONS,
        doc='/docs/mnZpbFGvT9zXjYF5'
        # All API metadatas
    )
