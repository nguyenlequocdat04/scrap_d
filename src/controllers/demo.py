import traceback

from datetime import datetime
from pymodm.errors import DoesNotExist

from src.models import DemoManager
from src.constants import AppConstants
from src.decorators import cache_function
from src.extensions import redis


class DemoController(object):
    @staticmethod
    def add(obj):
        return DemoManager.add(obj)

    @staticmethod
    def delete(_id):
        try:
            DemoManager.delete(_id)
        except DoesNotExist:
            return None

    @staticmethod
    def patch(_id, path_update):
        try:
            DemoManager.patch(_id, path_update)
            return {'result': True, 'msg': 'Done'}
        except DoesNotExist:
            return None

    @staticmethod
    @cache_function(60, 'cment')
    def get_list(sort_type, page, page_size):
        _sort = []
        if sort_type == 'oldest':
            _sort = [('_id', 1)]
        if sort_type == 'lastest':
            _sort = [('_id', -1)]
        _filter = {}
        try:
            datas, total = DemoManager.get_list(
                _filter, _sort, page, page_size
            )
            return {'data': list(datas), 'total': total}
        except DoesNotExist:
            return None

    @staticmethod
    @cache_function(600, 'cment')
    def get_by_id(_id):
        try:
            return DemoManager.get_by_id(_id)
        except DoesNotExist:
            return None

    @staticmethod
    def get_all():
        return list(DemoManager.get_all())
