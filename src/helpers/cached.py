import hashlib

from flask import request


class MyHelper(object):
    @staticmethod
    def get_key_to_hash(*args, **kwargs):
        """ Return key to hash for *args and **kwargs."""
        key_to_hash = ""
        # First args
        for i in args:
            key_to_hash += ":%s" % i
        # Attach any kwargs
        for key in sorted(kwargs):
            key_to_hash += ":%s" % kwargs[key]

        return key_to_hash

    @staticmethod
    def get_hash_key(prefix, key_to_hash):
        """ Return hash for a prefix and a key to hash."""
        key_to_hash = key_to_hash.encode('utf-8')
        key = prefix + ":" + hashlib.md5(key_to_hash).hexdigest()
        return key
