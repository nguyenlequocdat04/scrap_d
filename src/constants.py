class AppConstants(object):
    FPTPLAY_STATIC_SERVER = 'https://static.fptplay.net'
    DEFAULT_AVATAR = 'https://static.fptplay.net/static/img/share/structure/08_05_2015/default_user_icon08-05-2015_16g50-27.jpg'
    ADMIN_AVATAR = 'https://static.fptplay.net/static/img/share/video/27_08_2019/fpt-play-crop-tron27-08-2019_14g57-26.png'
