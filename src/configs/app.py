import os


class BaseConfig(object):
    PROJECT = "fplay-py3-simple"
    AUTHORIZATION_KEY = 'mytoken'


class DefaultConfig(BaseConfig):
    DEVELOP = os.getenv('DEVELOP') or True
    DEBUG = BUNDLE_ERRORS = DEVELOP

    APP_REDIS_URL = os.getenv('APP_REDIS_URL') \
        or 'redis://redis_services:6379/1'
    APP_MONGODB = os.getenv('APP_MONGODB') \
        or 'mongodb://mongodb_services:27017/sample'
    # SENTRY_DSN = os.getenv('SENTRY_DSN') \
    #     or 'http://75bb6509bba649759b16e3ff52c8b720:a62ea20396174c71963ddb0fb5cb76cf@sentry.adsplay.net/33'
    SENTRY_DSN = 'http://75bb6509bba649759b16e3ff52c8b720:a62ea20396174c71963ddb0fb5cb76cf@sentry.adsplay.net/33'
