from flask import request
from flask_restplus import Namespace, Resource, fields
from bson.objectid import ObjectId

from src.decorators import login_required, admin_required, get_user_info

from src.controllers import DemoController as ApiService
from src.meta.v1 import DemoModule as Module

api = Module.api

model_post = Module.model_post
model = Module.model
model_list = Module.model_list

api_parser = Module.api_parser


@api.route('')
@api.doc(responses=Module.RESPONSE_CODE)
class CommentList(Resource):
    @api.expect(api_parser)
    @api.marshal_with(model, skip_none=True)
    @get_user_info
    def get(self, user_info):
        """List data with `parser` provider - not require login
        """
        args = api_parser.parse_args()
        # res = ApiService.get_list(
        #     args['sort_type'],
        #     args['page'],
        #     args['page_size'],
        # )
        return 1/0

    @api.expect(model_post, validate=True)
    # @login_required
    def post(self):
        """Use for add new record with data valid from `model_post` - login required
        """
        obj = api.payload
        _id = ObjectId()
        obj['_id'] = str(_id)

        result = ApiService.add(obj)

        return {'_id': str(_id)}, 202


@api.route('/<id>')
@api.param('id', 'The comment identifier')
@api.doc(responses=Module.RESPONSE_CODE)
class Comment(Resource):
    @api.marshal_with(model, skip_none=True)
    @api.doc(security='authorization')
    # @login_required
    def get(self, id):
        if not ObjectId().is_valid(id):
            return {'msg': 'Id is not valid!'}, 400

        print(id, flush=True)
        obj = ApiService.get_by_id(id)
        if not obj:
            return {}, 404
        return obj

    @api.doc(security='authorization')
    @login_required
    def patch(self, id, user_info):
        '''use for exec action with comment [like, unlike, report] by id'''
        if not ObjectId().is_valid(id):
            return {'msg': 'Id is not valid!'}, 400

        obj_old = ApiService.get_by_id(id)
        if not obj_old:
            return {}, 404

        args = api_parser.parse_args()
        if args['action'] == 'report' and not args['desc']:
            return {}, 400

        args['user_id'] = user_info['user_id']
        res = ApiService.patch_with_action(id, args, obj_old)
        return res, 202

    @api.doc(security='authorization')
    @login_required
    def put(self, id):
        return {}

    @api.doc(security='authorization')
    @login_required
    def delete(self, id):
        return {}
