from src.meta.v2 import ApiModule

from .demo import api as demo

blueprint = ApiModule.blueprint
api = ApiModule.api

# routing api for app
api.add_namespace(demo, path='/sample')
