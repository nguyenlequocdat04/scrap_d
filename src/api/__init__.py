from .v1 import blueprint as api_v1
from .v2 import blueprint as api_v2


__all__ = ['api_v1', 'api_v2']
