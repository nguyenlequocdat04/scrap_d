FROM python:3.6-alpine

RUN apk add --no-cache tzdata && cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime \
    && echo "Asia/Ho_Chi_Minh" > /etc/timezone

RUN apk upgrade -U \
    && apk add --no-cache -u ca-certificates libva-intel-driver supervisor python3-dev build-base linux-headers pcre-dev \
    && rm -rf /tmp/* /var/cache/*

COPY requirements.txt /
RUN pip --no-cache-dir install -r requirements.txt && mkdir -p /var/log/apps

COPY uwsgi.ini /etc/uwsgi/
COPY server/ /etc/supervisor.d/
COPY . /webapps/fsample

WORKDIR /webapps/fsample