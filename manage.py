import os
import unittest

from src import create_app
from flask_script import Manager

app = create_app()
manager = Manager(app)

project_root_path = os.path.join(os.path.dirname(app.root_path))


@manager.command
def run(port=5000):
    print('app running..')
    app.run(host='0.0.0.0', port=port)


@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('fbone/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        return 0
    return 1


manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")

if __name__ == "__main__":
    manager.run()
