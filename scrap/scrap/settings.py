BOT_NAME = 'joneslanglasalle_spider'

SPIDER_MODULES = ['scrap.spiders']
NEWSPIDER_MODULE = 'scrap.spiders'


ITEM_PIPELINES = {
    'scrap.pipelines.MongoDBPipeline':300, 
    'scrap.pipelines.VirPipeline':300, 
    # 'scrap.pipelines.DuplicatesPipeline':300, 
}

MONGODB_SERVER = "172.18.0.2"
MONGODB_PORT = 27017
MONGODB_DB = "scrappy_datnlq"
MONGODB_COLLECTION = "vir"

DOWNLOAD_DELAY = 0.25
LOG_LEVEL = 'INFO'
LOG_ENABLE = False