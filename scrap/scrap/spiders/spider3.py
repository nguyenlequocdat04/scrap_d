import scrapy

# https://www.vir.com.vn/
url = 'https://www.vir.com.vn/'
class Vir(scrapy.Spider):
    name = 'vir_spider'
    start_urls = ['https://www.vir.com.vn/']
    custom_settings = {
        'ITEM_PIPELINES': {
            'scrap.pipelines.VirPipeline':300, 
        }
    }
    def parse(self, response):
        # follow links to content pages
        count = 0
        for href in response.css('.article .title::attr(href)').getall():
            print(href)
            count += 1
            yield response.follow(href, self.parse_content)

        # follow pagination links
        # for href in response.css('li.next a::attr(href)'):
        #     yield response.follow(href, self.parse)

    def parse_content(self, response):
        def extract_with_css(query, getall=False):
            if getall:
                return response.css(query).getall()
            return response.css(query).get(default='').strip()

        yield {
            'url': response.url,
            'title': extract_with_css('.title-detail::text'),
            'description': extract_with_css('.desc-detail::text'),
            'content': extract_with_css('.htmlContent p::text', getall=True),
            'who': 'datnlq'
        }