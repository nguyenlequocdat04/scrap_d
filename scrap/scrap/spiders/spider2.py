import scrapy
# https://www.joneslanglasalle.com.vn/en/trends-and-insights
url = 'https://www.joneslanglasalle.com.vn'
class Joneslanglasalle(scrapy.Spider):
    name = 'joneslanglasalle_spider'
    start_urls = ['https://www.joneslanglasalle.com.vn/en/trends-and-insights']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrap.pipelines.MongoDBPipeline':300, 
        }
    }
    def parse(self, response):
        # follow links to content pages
        for href in response.css('[class*="col-"] a::attr(href)').getall():
            yield response.follow(url+href, self.parse_content)



    #     # follow pagination links
    #     for href in response.css('li.next a::attr(href)'):
    #         yield response.follow(href, self.parse)

    def parse_content(self, response):
        def extract_with_css(query, getall=False):
            if getall:
                return response.css(query).getall()
            return response.css(query).get(default='').strip()

        yield {
            'url': response.url,
            'title': extract_with_css('.title h1 *::text'),
            'description': extract_with_css('.description p::text'),
            'content': extract_with_css('.richtext p::text', getall=True),
        }