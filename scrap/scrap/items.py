# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class ScrapItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class JoneslanglasalleItem(Item):
    url = Field()
    title = Field()
    description = Field()
    content = []