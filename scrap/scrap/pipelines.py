import pymongo

# from scrapy.conf import settings
from scrapy.exceptions import DropItem
# from scrapy import log
import logging
from scrapy.utils.project import get_project_settings
settings = get_project_settings()
logger = logging.getLogger()
class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]
    
    def process_item(self, item, spider):
        for data in item:
            if not data:
                raise DropItem("Missing data!")
        if not self.collection.find_one({'url': item['url']}):
            self.collection.insert(dict(item))
            logger.info(f"added {item['url']}!")
        else:
            logger.warning(f"duplicate url: {item['url']}")
        return item


class VirPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db['vir_db2']
    
    def process_item(self, item, spider):
        for data in item:
            if not data:
                raise DropItem("Missing data!")
        if not self.collection.find_one({'url': item['url']}):
            self.collection.insert(dict(item))
            logger.info(f"added {item['url']}!")
        else:
            logger.warning(f"duplicate url: {item['url']}")
        return item
